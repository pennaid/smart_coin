const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

const modelName = 'User';

const schema = new Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
}, { timestamps: true });

schema.pre('save', function preSaveUser(next) {
  const user = this;

  if (!user.isModified('password')) {
    next();
  } else {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (error, hash) => {
        user.password = hash;
        next();
      });
    });
  }
});

schema.methods.comparePassword = function comparePassword(password, cb) {
  bcrypt.compare(password, this.password, (err, match) => {
    if (err) return cb(err);
    return cb(null, match);
  });
};

let model;
try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);
}

module.exports = { model };
