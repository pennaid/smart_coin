## Models

#### trades

Persist all bitcoin trades for a given exchange.

```
bitfinex: [{
    time: 125123412,
    price: 123.43,
    amount: -13.53,
  }, {}, ...
]

```


#### orderbook

Persist exchange's orderbook (each access should occur each minute).

```
bitfinex: [
  {
    created_at: 123498159,
    asks: [[ price, amount ], ...],
    bids: [[ price, amount ], ...]
  }, {
    ...
  },
  ...
]
```

------------- OR -------------

```
bitfinex: {
  id1: {
    created_at: 12349000,
    asks: [[ price, amount ], ...],
    bids: [[ price, amount ], ...]
  },
  id2: {
    created_at: 123498159,
    asks: [[ price, amount ], ...],
    bids: [[ price, amount ], ...]
  }
}
```

Maybe `id` being the time?



# DON'T DO METHODS YET

### Methods

We are not going to retrieve the raw trades. Instead, we are using it to build candles, which are useful for our needs.

#### getCandle(size, period)


That being said, we will have an interface to build the candles or we will use methods from the schema.

**size:** how many candles should be returned.

**period:** The candle should start at a moment and end after the period has passed.

| period   | Time    |
| ------- | --------- |
| minute  | 1 MINUTE  |
| hour    | 1 HOUR    |
| day     | 1 DAY     |

**RETURN**

Should return an array of candles.

```
[{
  time: 1447533963511,
  period: "minute",
  open: 34.19000000,
  close: 34.00000000,
  high: 95.70000000,
  low: 7.06000000
}, {
  time: 1447534963511,
  period: "minute",
  open: 34.00000000,
  close: 34.10000000,
  high: 97.60000000,
  low: 10.06000000
},
...
]
```

**Example**

Calling `getCandles(5, 'day')` will return 5 candles built for a period of 1 day. That is:

`time`: is the starting time for the candle (the time of the `open` trade)
`close`: the price of the last trade
`open`: the price of trade 24h before the trade used for `close`
`high`: the highest price in the trades occurred between the `close` and `open` trades.
`low`: the lowest price in the trades occurred between the `close` and `open` trades.
