const mongoose = require('mongoose');
const { logger } = require('../helpers');

const { Schema } = mongoose;

const modelName = 'Settings';

const schema = new Schema({
  exchange: { type: String, required: true, unique: true },
  exchangeName: { type: String, required: true },
  wmaLength: { type: Number },
  emaLength: { type: Number },
  hullLength: { type: Number },
  bbMult: { type: Number },
  bbLength: { type: Number },
  minLeverage: { type: Number, default: 2, min: 0, max: 100 },
  maxContractSize: { type: Number, default: 2000 },
  maxAssetSize: { type: Number, default: 0.3, min: 0, max: 100 },
  minContractSize: { type: Number, default: 1, min: 1 },
  minAssetSize: { type: Number, default: 0.00001, min: 0.00001 },
}, { timestamps: true });

function findByExchange({ exchange }) {
  const self = this;
  return self.findOne({ exchange });
}

schema.statics.findByExchange = findByExchange;

let model;
try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);

  model.find({ exchange: 'BMX' })
    .then((bmx) => {
      if (!bmx.length) model.create({ exchange: 'BMX', exchangeName: 'BitMEX' });
    })
    .catch((err) => {
      logger.error('app.boot.mongo :: Could not check exchange BMX initialization.', err);
      throw Error('app.boot.mongo :: Could not check exchange BMX initialization. Check log for details.');
    });
}

module.exports = { model };
