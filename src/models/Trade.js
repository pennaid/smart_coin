const _ = require('lodash');
const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelName = 'Trade';

const schema = new Schema({
  exchange: { type: String, required: true },
  time: { type: Number, required: true },
  price: { type: Number, required: true },
  amount: { type: Number, required: true },
  virtualId: String,
  id: String,
}, { timestamps: true });

function preSave(next) {
  const self = this;
  const { time, price, amount } = self;

  self.virtualId = `${time}-${price}-${amount}`;

  return next();
}

schema.pre('save', preSave);

function findByExchange(exchange) {
  const self = this;

  return self.find({ exchange });
}

async function getCandles(exchange, duration, numOfCandles, sort = 'asc') {
  const translate = {
    day: 86400000,
    hour: 3600000,
    minute: 60000,
    second: 1000,
  };

  const self = this;
  let tradeOrders = await self.find({ exchange }).sort({ time: 'desc' });

  const startTime = tradeOrders[0].time;

  tradeOrders = tradeOrders
    .filter((tradeOrder) =>
      (tradeOrder.time >= (startTime - (translate[duration] * numOfCandles))));

  const candlesData = tradeOrders.reduce((result, tradeOrder) => {
    const index =
      Math.abs(Math.floor((startTime - tradeOrder.time) / (translate[duration])));

    const newResult = [...result];
    if (Array.isArray(newResult[index])) newResult[index].push(tradeOrder);
    else {
      newResult[index] = [tradeOrder];
    }

    return newResult;
  }, []);

  const candles = candlesData.map((tOrders) => {
    if (tOrders) {
      const max = _.maxBy(tOrders, (t) => t.price);
      const min = _.minBy(tOrders, (t) => t.price);

      const open = _.minBy(tOrders, (t) => t.time);
      const close = _.maxBy(tOrders, (t) => t.time);

      return {
        open: open.price,
        close: close.price,
        high: max.price,
        low: min.price,
        dates: [open.time, close.time],
      };
    }

    return null;
  }).filter((candle) => candle);

  return sort === 'asc' ? candles : candles.reverse();
}

schema.statics.getCandles = getCandles;
schema.statics.findByExchange = findByExchange;

let model;
try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);
}

module.exports = { model };
