const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelName = 'Candle';

const schema = new Schema({
  time: { type: Number, required: true },
  low: { type: Number, required: true },
  high: { type: Number, required: true },
  open: { type: Number, required: true },
  close: { type: Number, required: true },
  volume: { type: Number, required: true },

  granularity: { type: String, required: true },
  exchange: { type: String, required: true },
}, { timestamps: true });

let model;
try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);
}

module.exports = { model };
