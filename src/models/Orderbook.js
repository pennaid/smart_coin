const mongoose = require('mongoose');

const { Schema } = mongoose;

const modelName = 'Orderbook';

const asksBidsSchema = new Schema({
  price: { type: Number, required: true },
  amount: { type: Number, required: true },
}, { _id: false });

const schema = new Schema({
  exchange: { type: String, required: true },
  asks: { type: [asksBidsSchema], default: [] },
  bids: { type: [asksBidsSchema], default: [] },
}, { timestamps: true });

function findByExchange(exchange) {
  const self = this;

  return self.find({ exchange });
}

schema.statics.findByExchange = findByExchange;

let model;
try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);
}

module.exports = { model };
