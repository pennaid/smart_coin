const cipher = require('../helpers/cipher');
const mongoose = require('mongoose');
const logger = require('../helpers/logger');

const { Schema } = mongoose;

const modelName = 'Client';

const schema = new Schema({
  key: { type: String, required: true },
  secret: { type: String, required: true },
  active: { type: Boolean, required: true, default: true },
  accountId: { type: Number, required: true, unique: true },
  test: { type: Boolean, required: true, default: true },
  firstname: { type: String },
  lastname: { type: String },
  email: { type: String },
  phone: { type: String },
  country: { type: String },
  label: { type: String },
  leverage: { type: Number, default: 5, min: 0, max: 100 },
  budget: { type: Number, default: 0.92, max: 1, min: 0 },
}, { timestamps: true });

function preSave(next) {
  const self = this;
  const { key, secret } = self;

  self.key = cipher.encrypt(key);
  self.secret = cipher.encrypt(secret);

  return next();
}


async function getAllClients() {
  const self = this;
  try {
    return await self.find({}).select('label accountId firstname lastname email phone country leverage test');
  } catch (e) {
    logger.error('Client.getAllClients failed -> ', e);
    return null;
  }
}

async function getActiveClients() {
  const self = this;
  try {
    const clients = await self.find({ active: true }).select('key secret accountId leverage test');
    return clients.map((client) => ({
      ...client._doc,
      key: cipher.decrypt(client.key),
      secret: cipher.decrypt(client.secret),
    }));
  } catch (e) {
    logger.error('Client.getAllClients failed -> ', e);
    return null;
  }
}

async function getPrivateClient(accountId) {
  const self = this;
  try {
    const client = await self.findOne({ accountId }).select('key secret firstname lastname accountId leverage budget test');
    return {
      ...client._doc,
      key: cipher.decrypt(client.key),
      secret: cipher.decrypt(client.secret),
    };
  } catch (e) {
    logger.error('Client.getPrivateClient failed -> \n', e);
    return null;
  }
}

schema.pre('save', preSave);
schema.statics.getAllClients = getAllClients;
schema.statics.getPrivateClient = getPrivateClient;
schema.statics.getActiveClients = getActiveClients;

let model;

try {
  model = mongoose.model(modelName);
} catch (e) {
  model = mongoose.model(modelName, schema);
}

module.exports = { model };
