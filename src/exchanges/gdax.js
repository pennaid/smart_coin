const WebSocket = require('ws');

const { logger } = require('../helpers');
const { model: Trade } = require('../models/Trade');

const pair = 'BTC-USD';

const client = new WebSocket('wss://ws-feed.gdax.com');

const extractTrade = (data) => {
  const { side, trade_id, time, price, last_size } = data;

  if (price && time) {
    const timestamp = new Date(time).getTime();
    const trade = new Trade({
      exchange: 'gdax',
      time: timestamp,
      price: Number(price) * (side === 'buy' ? 1 : -1),
      amount: Number(last_size),
      id: trade_id,
    });
    // logger.info(`GDAX new filled trade Time: ${time} :: Price => ${price} :: Amount => ${trade.amount}`);
    return trade;
  }

  return null;
};

const subscribe = JSON.stringify({
  type: 'subscribe',
  product_ids: [pair],
  channels: ['ticker'],
});

client.on('open', () => {
  client.send(subscribe);
});

client.on('message', (data) => {
  const message = JSON.parse(data);
  const trade = extractTrade(message);
  if (trade) trade.save();
});
