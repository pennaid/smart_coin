// API_KEY = "grZDPbzKkHQtbcZ-xLDBPrsS"
// API_SECRET = "aTxISReoYb8OlEeCZOCJ1QYId3o4-cBMWMy5d65ZAS_LXps8"
/*
{
  key: 'grZDPbzKkHQtbcZ-xLDBPrsS',
  secret: 'aTxISReoYb8OlEeCZOCJ1QYId3o4-cBMWMy5d65ZAS_LXps8'
}
*/

/* eslint-disable max-len */
const axios = require('axios');
const crypto = require('crypto');

const { logger } = require('../../helpers');

const { BITMEX_PROD } = process.env;

const apiVersion = '/api/v1';
const baseUrl = BITMEX_PROD === 'true' ? `https://www.bitmex.com${apiVersion}` :
  `https://testnet.bitmex.com${apiVersion}`;

const PAIR = 'XBTUSD';
let lastNonce = 0;

const encodeParams = (params) => {
  const keys = Object.keys(params);
  return keys.reduce((acc, key) => {
    if (!params[key]) return acc;
    const encodedParams = typeof params[key] === 'string' ? params[key] : encodeURIComponent(JSON.stringify(params[key]));
    return `${acc}${key}=${encodedParams}&`;
  }, '?');
};

const getAuthentication = ({ key, secret, data, params, path, method } = {}) => {
  const now = Date.now();
  const nonce = now > lastNonce ? now : lastNonce + 1;
  // console.log('lastNonce, nonce', lastNonce, nonce);
  lastNonce = nonce;
  const message = method + apiVersion + path + params + nonce + (data ? JSON.stringify(data) : '');
  return {
    'api-nonce': nonce,
    'api-key': key,
    'api-signature': crypto.createHmac('sha256', secret).update(message).digest('hex'),
  };
};

const sendPrivate = async({ key, secret, data, method, path, params } = {}) => {
  try {
    const start = Date.now();
    const encodedParams = params ? encodeParams(params) : '';
    const authentication = getAuthentication({ key, secret, data, path, method, params: encodedParams });
    console.log('\n', `\x1b[36mPrivate ${method}\x1b[0m`, path, '\n', params || data, '\n');
    const response = await axios({
      url: `${baseUrl}${path}${encodedParams ? `${encodedParams}` : ''}`,
      method,
      headers: { ...authentication },
      data,
    });

    console.log(`sendPrivate done ${method}: `, Date.now() - start);

    return response;
  } catch (e) {
    logger.error(`exchanges.bitmex.${path} :: ${e}
      Message: ${e.response && e.response.data.error.message}
      Data:`, e.reponse ? e.response.data : 'undefined');
    return null;
  }
};

const sendPublic = async({ method, path, data, params } = {}) => {
  try {
    const response = await axios({
      url: `${baseUrl}${path}`,
      method,
      data,
      params,
    });
    console.log('\n', `\x1b[36mPublic ${method}\x1b[0m`, path, '\n', params || data, '\n');
    return response;
  } catch (e) {
    logger.error(`exchanges.bitmex.${path} :: ${e}
      Message: ${e.response.data.error.message}
      Data:`, e.response.data);
    return null;
  }
};

/** ******** PUBLIC METHODS ******** */

// TODO RETURN SIZE IN CONTRACTS (how to change it?)
const getOrderbook = async({ pair = PAIR, depth = 50 } = {}) => {
  const method = 'GET';
  const path = '/orderBook/L2';
  const params = { symbol: pair, depth };

  try {
    const orderbook = await sendPublic({ method, path, params });
    return orderbook.reduce((acc, order) => {
      if (order.side === 'Buy') acc.bids.push([order.price, order.size]);
      else acc.asks.push([order.price, order.size]);
      return acc;
    }, { asks: [], bids: [] });
  } catch (e) {
    return e;
  }
};

const getCandles = ({ pair = PAIR, interval = 3600, count = 129, partial = true } = {}) => {
  const intervalMap = {
    60: '1m',
    300: '5m',
    3600: '1h',
    86400: '1d',
  };
  const method = 'GET';
  const path = '/trade/bucketed';
  const params = {
    binSize: intervalMap[interval],
    symbol: pair,
    partial,
    count,
    reverse: true,
  };
  return sendPublic({ params, method, path });
};

const getInstrument = ({ pair = PAIR } = {}) => {
  const method = 'GET';
  const path = '/instrument';
  const params = {
    symbol: pair,
    count: 1,
    reverse: true,
  };
  return sendPublic({ params, method, path })
    .then((res) => res.data[0]);
};

/** ******************** PRIVATE METHODS ******************** */
/** ******** Key and Secret required for all methods ******** */

// Build order with provided data
// DON'T USE FOR GET METHODS
function buildOrderFunction({ key, secret, data, method: m, path: p }) {
  const method = m || 'POST';
  const path = p || '/order';

  if (this.bulk) {
    this.bulk.push(data);
    return this;
  }
  return sendPrivate({ key, secret, data, method, path });
}

this.buildOrder = buildOrderFunction.bind(this);

// Create a bulk order.
// Orders must be chained after calling this function.
// Call sendBulkOrder() to send orders.
const createBulkOrder = function ({ key, secret }) {
  Object.assign(this, module.exports, {
    buildOrder: buildOrderFunction.bind(this),
    bulk: [],
    key,
    secret,
    method: 'POST', // Change automatic to PUT for update method
  });
  return this;
};

// Send orders defined in bulk.
const sendBulkOrder = async function () {
  if (!this.bulk || !this.bulk.length) throw Error('Missing bulk. Start bulk order with createBulkOrder and chain each order.');
  const { method } = this;
  const path = '/order/bulk';
  const data = { orders: this.bulk };
  const response = await sendPrivate({ key: this.key, secret: this.secret, data, method, path });
  this.bulk = null;
  return response;
};

// Required params: assetAmount/contractAmount
const sendMarketOrder = function ({ key, secret, pair = PAIR, assetAmount, contractAmount, clientOrderId, instructions, note }) {
  const execInst = Array.isArray(instructions) ? instructions.join(',') : instructions;
  const data = {
    symbol: pair.toUpperCase(),
    ordType: 'Market',
    orderQty: contractAmount,
    simpleOrderQty: assetAmount,
    clOrdID: clientOrderId,
    execInst,
    text: note,
  };
  return this.buildOrder({ key, secret, data });
};

// Required params: assetAmount/contractAmount, price
const sendLimitOrder = function ({ key, secret, pair = PAIR, price, assetAmount, contractAmount, clientOrderId, instructions, note }) {
  const execInst = Array.isArray(instructions) ? instructions.join(',') : instructions;
  const data = {
    symbol: pair.toUpperCase(),
    ordType: 'Limit',
    simpleOrderQty: assetAmount,
    orderQty: contractAmount,
    clOrdID: clientOrderId,
    price,
    execInst,
    text: note,
  };
  return this.buildOrder({ key, secret, data });
};

// Use a price below the current price for stop-sell orders.
// Required params: triggerPrice, assetAmount/contractAmount, price (for Limit)
const sendStopOrder = function ({ key, secret, type = 'market', pair = PAIR, triggerPrice, price, contractAmount, assetAmount, instructions, clientOrderId, note }) {
  const execInst = !instructions ? 'LastPrice' : // eslint-disable-line no-nested-ternary
    (Array.isArray(instructions) ? instructions.join(',') : instructions);
  const ordType = type === 'market' ? 'Stop' : 'StopLimit';
  const data = {
    symbol: pair.toUpperCase(),
    ordType,
    price,
    stopPx: triggerPrice,
    orderQty: contractAmount,
    simpleOrderQty: assetAmount,
    clOrdID: clientOrderId,
    text: note,
    execInst,
  };
  return this.buildOrder({ key, secret, data });
};

// Use a price below the current price for buy-if-touched orders.
// Required params: triggerPrice, assetAmount/contractAmount, price (for Limit)
const sendIfTouchedOrder = function ({ key, secret, type = 'market', pair = PAIR, triggerPrice, price, contractAmount, assetAmount, instructions, clientOrderId, note }) {
  const execInst = !instructions ? 'LastPrice' : // eslint-disable-line no-nested-ternary
    (Array.isArray(instructions) ? instructions.join(',') : instructions);
  const ordType = type === 'market' ? 'MarketIfTouched' : 'LimitIfTouched';
  const data = {
    symbol: pair.toUpperCase(),
    ordType,
    price,
    stopPx: triggerPrice,
    orderQty: contractAmount,
    simpleOrderQty: assetAmount,
    clOrdID: clientOrderId,
    text: note,
    execInst,
  };
  return this.buildOrder({ key, secret, data });
};

const updateOrder = function ({ key, secret, orderId, clientOrderId, price, stopPrice, contractAmount, assetAmount, note }) {
  const method = 'PUT';
  this.method = method; // for bulk order
  const data = {
    orderID: orderId,
    clOrdID: clientOrderId,
    orderQty: contractAmount,
    simpleOrderQty: assetAmount,
    price,
    stopPx: stopPrice,
    text: note,
  };
  return this.buildOrder({ key, secret, data, method });
};

const getOpenOrders = ({ key, secret, pair = PAIR } = {}) => {
  const method = 'GET';
  const path = '/order';
  const params = { filter: { open: true }, symbol: pair };
  return sendPrivate({ key, secret, pair, method, path, params });
};

// Get all orders or filtered by ID (string or array).
// @param limit number of orders to retrieve
const getOrders = async({ key, secret, pair = PAIR, limit = 10, filter } = {}) => {
  const method = 'GET';
  const path = '/order';
  const params = {
    count: limit,
    symbol: pair,
    reverse: true,
  };
  if (filter) params.filter = filter;
  return sendPrivate({ key, secret, params, method, path });
};

const getOrdersByIds = async({ key, secret, orderId, clientOrderId, pair = PAIR } = {}) => {
  if (orderId && clientOrderId) throw Error('Bitmex: <getOrdersByIds> Pass only orderId or clientOrderId, not both.');

  try {
    const filter = orderId ? { orderID: orderId } : { clOrdID: clientOrderId };
    return getOrders({ key, secret, pair, filter });
  } catch (e) {
    logger.error('Bitmex: <getOrdersByIds> Error', e);
    return null;
  }
};

// Get orders status as text ('New', 'Filled'...)
// Status dictionary: http://www.onixs.biz/fix-dictionary/5.0.SP2/tagNum_39.html
const getOrdersStatus = async({ key, secret, orderId, clientOrderId, pair = PAIR } = {}) => {
  if (orderId && clientOrderId) throw Error('Bitmex: <getOrdersStatus> Pass only orderId or clientOrderId, not both.');

  try {
    const orders = await getOrders({ key, secret, orderId, clientOrderId, pair });
    const property = clientOrderId ? 'clOrdID' : 'orderID';
    return orders.reduce((acc, order) => ({ ...acc, [order[property]]: order.ordStatus }), {});
  } catch (e) {
    logger.error('error :: exchanges.bitmex.checkOrdersByIds -> \n', e);
    return null;
  }
};

// Required params: orderId OR clientOrderId, not both
// @params orderId String OR Array of strings
// @params clOrdID String OR Array of strings
const cancelOrder = function ({ key, secret, orderId, clientOrderId, note }) {
  if (orderId && clientOrderId) throw Error('Bitmex: <cancelOrder> Pass only orderId or clientOrderId, not both.');
  if (!orderId && !clientOrderId) throw Error('Bitmex: <cancelOrder> Either orderId or clientOrderId must be provided.');
  const method = 'DELETE';
  const data = {
    orderID: orderId,
    clOrdID: clientOrderId,
    text: note,
  };
  return this.buildOrder({ key, secret, data, method });
};


const cancelAllOrders = function ({ key, secret, pair = PAIR, filter, note }) {
  const method = 'DELETE';
  const path = '/order/all';
  console.log('cancel all');
  const data = {
    filter,
    symbol: pair,
    text: note,
  };
  return sendPrivate({ key, secret, data, method, path });
};

const cancelAllBuyOrders = (args) => cancelAllOrders({ ...args, filter: { side: 'Buy' } });

const cancelAllSellOrders = (args) => cancelAllOrders({ ...args, filter: { side: 'Sell' } });

const getPositions = ({ key, secret, pair = PAIR } = {}) => {
  const method = 'GET';
  const path = '/position';
  const pairParam = pair ? { symbol: pair } : null;
  const params = { filter: { isOpen: true, ...pairParam } };
  return sendPrivate({ key, secret, params, method, path });
};

// @params leverage number 0.01 - 100. Send 0 to enable cross margin.
const setLeverage = ({ key, secret, pair = PAIR, leverage } = {}) => {
  if (!leverage && leverage !== 0) throw Error('bitmex.setLeverage :: leverage param should be set');
  const method = 'POST';
  const path = '/position/leverage';
  const data = {
    symbol: pair,
    leverage,
  };
  return sendPrivate({ key, secret, data, method, path });
};

// @params riskLimit number New Risk Limit, in Satoshis.
const setRiskLimit = ({ key, secret, pair = PAIR, riskLimit } = {}) => {
  if (!riskLimit) throw Error('bitmex.setRiskLimit :: riskLimit param should be set');
  const method = 'POST';
  const path = '/position/riskLimit';
  const data = {
    symbol: pair,
    riskLimit,
  };
  return sendPrivate({ key, secret, data, method, path });
};

// Set margin amount for position. Automatically isolate position margin
// @params amount number Amount to add or subtract (negative) from position margin in Satoshis
const setMarginAmount = ({ key, secret, pair = PAIR, amount } = {}) => {
  if (!amount) throw Error('bitmex.setRiskLimit :: amount param should be set');
  const method = 'POST';
  const path = '/position/transferMargin';
  const data = {
    symbol: pair,
    amount,
  };
  return sendPrivate({ key, secret, data, method, path });
};

/*
  accountId: account id,
  riskLimit: setted risk limit,
  walletBalance: value in wallet (don't count unrealised value) (Satoshis)
  marginBalance: how much of wallet is available for margin (wallet - unrealised PNL) (Satoshis)
  marginLeverage: actual leverage for position
  marginUsedPcnt: what is percentege of the available leveraged margin is being used
  marginAvailable: how much is there available for margin use (Satoshis)
*/
const getMargin = async({ key, secret, pair = 'XBt' } = {}) => {
  const method = 'GET';
  const path = '/user/margin';
  const params = { currency: pair };
  return sendPrivate({ key, secret, params, method, path })
    .then((r) => r.data);
};

const getUser = ({ key, secret } = {}) => {
  const method = 'GET';
  const path = '/user';
  return sendPrivate({ key, secret, method, path })
    .then((r) => r.data);
};

module.exports = {
  sendMarketOrder,
  sendLimitOrder,
  sendStopOrder,
  sendIfTouchedOrder,

  updateOrder,

  getOrders,
  getOrdersByIds,
  getOrdersStatus,
  getOpenOrders,
  getOrderbook,
  getCandles,
  getInstrument,

  cancelOrder,
  cancelAllOrders,
  cancelAllBuyOrders,
  cancelAllSellOrders,

  getUser,
  getMargin,
  getPositions,

  setLeverage,
  setRiskLimit,
  setMarginAmount,

  createBulkOrder,
  sendBulkOrder,
};

// getCandles({})
//   .then((d) => console.log(d.length))
//   .catch((e) => console.log(e));

// updateOrder({
//   key: 'grZDPbzKkHQtbcZ-xLDBPrsS',
//   secret: 'aTxISReoYb8OlEeCZOCJ1QYId3o4-cBMWMy5d65ZAS_LXps8',
//   price: 5100,
//   // contractAmount: -500,
//   orderId: '12edc7c5-c322-d528-0d70-eefe9c9ba686',
// })
// .then(console.log);

// sendLimitOrder({
//   key: 'grZDPbzKkHQtbcZ-xLDBPrsS',
//   secret: 'aTxISReoYb8OlEeCZOCJ1QYId3o4-cBMWMy5d65ZAS_LXps8',
//   price: 5500,
//   contractAmount: 100,
//   instructions: 'ParticipateDoNotInitiate',
// })
//   .then(console.error);

// 98e42e36-fc26-d473-8955-84e0853b7a97

// createBulkOrder({
//   key: 'grZDPbzKkHQtbcZ-xLDBPrsS',
//   secret: 'aTxISReoYb8OlEeCZOCJ1QYId3o4-cBMWMy5d65ZAS_LXps8',
// })
// // .sendBuyOrderMarket({ assetAmount: 0.1, note: 'second' })
// // .sendBuyOrderMarket({ assetAmount: 0.1, note: 'third' })
// .sendBulkOrder();
