const axios = require('axios');
const moment = require('moment');

const { model: Candle } = require('../../models/Candle');

const API_URL = 'https://api.gdax.com';
const PAIR = 'BTC-USD';

const getCandles = async(num = 32, gran = 14400) => {
  const end = moment();
  const start = moment().subtract(num * gran, 'seconds');

  const query = {
    method: 'GET',
    url: `${API_URL}/products/${PAIR}/candles`,
    params: {
      start: start.toDate(),
      end: end.toDate(),
      granularity: gran,
    },
  };

  try {
    const candles = (await axios(query)).data;

    Promise.all(candles.map(async(candle, i) => {
      const FIXING_TIME = Number(`${candle[0]}000`); // I HATE YOU GDAX;

      const time = await Candle.findOne({ time: FIXING_TIME, exchange: 'gdax', granularity: 14400 });

      if (i === candles.length - 1) {
        if (time) await Candle.remove({ time: FIXING_TIME, exchange: 'gdax', granularity: 14400 });
      }

      if (!time && i !== candles.length - 1) return null;

      return Candle.create({
        time: FIXING_TIME,
        low: Number(candle[1]),
        high: Number(candle[2]),
        open: Number(candle[3]),
        close: Number(candle[4]),
        volume: Number(candle[5]),

        exchange: 'gdax',
        granularity: gran,
      });
    }));

    return candles;
  } catch (e) { return null; }
};

getCandles();

module.exports = { getCandles };
