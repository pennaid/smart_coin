const Pusher = require('pusher-js');

const { logger } = require('../helpers');
const { model: Trade } = require('../models/Trade');

const client = new Pusher('de504dc5763aeef9ff52', { cluster: 'mt1' });

client.subscribe('live_trades');

const extractTrade = (data) => {
  const { timestamp, id, type, amount, price } = data;

  if (price && amount) {
    const time = new Date(Number(`${timestamp}000`)).getTime();

    const trade = new Trade({
      exchange: 'bitstamp',
      time,
      price: Number(price) * (type ? -1 : 1),
      amount: Number(amount),
      id,
    });

    // logger.info(`Bitstamp new filled trade Time: ${new Date(time)} :: Price => ${price} :: Amount => ${amount}`);

    return trade;
  }

  return null;
};

client.bind('trade', (data) => {
  const trade = extractTrade(data);
  if (trade) trade.save();
});
