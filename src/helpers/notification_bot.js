const { logger } = require('../helpers');

const validIds = [];

class NotificationBot {
  constructor({ bot } = {}) {
    if (!bot) throw Error('Bot not found.');

    this.bot = bot;
    this.listeningGroups = validIds;
  }

  static validateId(id) {
    return (validIds.indexOf(id) !== -1);
  }

  async sendMessage(text, options = {}) {
    const self = this;
    const { logType = 'info' } = options;
    const { listeningGroups, bot } = self;

    await Promise.all(listeningGroups.map((group) => bot.sendMessage(group, `${logType} :: ${text}`)))
      .catch((e) => logger.error(`Couldn't send message ::\n${e}`));
  }

  addToListenersAction(command) {
    const self = this;

    self.bot.command(`/${command}`, (ctx, next) => {
      const { id: chatId } = ctx.chat;

      if (!NotificationBot.validateId(chatId)) return next();

      const foundChat = self.listeningGroups.find((id) => id === chatId);

      if (!foundChat && validIds.indexOf(chatId) !== -1) {
        self.listeningGroups.push(chatId);
      }

      return next();
    });
  }

  addCommand(command, callback) {
    const self = this;

    self.bot.command(`/${command}`, async(_, next) => {
      await callback();
      return next();
    });
  }

  addReplyCommand(command, messagesFunc) {
    const self = this;

    self.bot.command(`/${command}`, async(ctx, next) => {
      const action = ctx.message.text.split(`/${command}`)[1].replace(' ', '');
      let messages;
      if (action === '') messages = await messagesFunc(ctx);
      else messages = await messagesFunc(ctx, { timeframe: action });
      if (!Array.isArray(messages)) messages = [messages];
      await ctx.reply(messages.join('\n'));
      return next();
    });
  }
}

module.exports = NotificationBot;
