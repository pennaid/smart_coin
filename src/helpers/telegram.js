const { logger } = require('../helpers');

module.exports = (bot) => {
  const sendMessage = (message, logType = 'info') => {
    if (!bot) return () => {};
    return bot.sendMessage(process.env.NOTIFICATION_BOT_GROUP, `${logType} :: ${message}`)
      .catch((e) => logger.error('Error sending message:\n', e));
  };

  return { sendMessage };
};
