const winston = require('winston');
const moment = require('moment');

winston.emitErrs = true;

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: () => moment().format('YYYY-MM-DD hh:mm:ss A'),
      humanReadableUnhandledException: true,
    }),
  ],
  exitOnError: false,
});

module.exports = logger;
