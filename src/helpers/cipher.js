const crypto = require('crypto');

const { ENCRYPTION_KEY } = process.env; // Must be 256 bytes (32 characters)



function encrypt(text) {
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
  const updated = cipher.update(text);

  const encrypted = Buffer.concat([updated, cipher.final()]);

  return `${iv.toString('hex')}:${encrypted.toString('hex')}`;
}

function decrypt(encrypted) {
  const encryptedParts = encrypted.split(':');
  const iv = Buffer.from(encryptedParts.shift(), 'hex');
  const encryptedText = Buffer.from(encryptedParts.join(':'), 'hex');

  const decipher = crypto.createDecipheriv('aes-256-cbc', ENCRYPTION_KEY, iv);
  const udpated = decipher.update(encryptedText);

  const decrypted = Buffer.concat([udpated, decipher.final()]);

  return decrypted.toString();
}
module.exports = { decrypt, encrypt };
