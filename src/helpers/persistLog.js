const fs = require('fs');
const path = require('path');
const logger = require('./logger');

const filePath = path.join(__dirname, '../advice_logs.log');

module.exports = function persistLog(data) {
  const { enter, close } = data;
  console.log('mandou persist', enter, close);
  fs.writeFile(
    path.join(filePath),
    `${(new Date()).toUTCString()} -> ENTER: ${enter} :: CLOSE: ${close}\n`,
    { flag: 'a' },
    (err) => {
      if (err) logger.error('Error persisting advice', err);
      logger.info('Persisted advice');
    },
  );
};
