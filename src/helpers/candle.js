const logger = require('./logger');
const { model: CandlePersistent } = require('../models/Candle');

class Candle {
  constructor(
    { high, low, open, close, openTimestamp, closeTimestamp, exchange, volume } = {},
    complete,
  ) {
    this.high = high || 0;
    this.low = low || 0;
    this.open = open || 0;
    this.close = close || 0;
    this.closeTimestamp = closeTimestamp || null;
    this.openTimestamp = openTimestamp || null;
    this.complete = complete || null;
    this.exchange = exchange || false;
    this.granularity = null;
    this.volume = volume || 0;
  }

  setGranularity(granularity) {
    if (!granularity) throw new Error('granularity is required.');
    this.granularity = granularity;
    return this;
  }

  setHigh(highValue) {
    if (!highValue) throw Error('highValue is required.');

    if (!this.complete) this.high = highValue;
    return this;
  }

  setLow(lowValue) {
    if (!lowValue) throw Error('lowValue is required.');

    if (!this.complete) this.low = lowValue;
    return this;
  }

  setVolume(volumeValue) {
    if (!volumeValue) throw Error('volumeValue is required.');

    if (!this.complete) this.volume = volumeValue;
    return this;
  }

  setOpen(openValue, timestamp = null) {
    if (!openValue) throw Error('openValue is required.');

    if (!this.complete) {
      this.open = openValue;
      this.openTimestamp = timestamp || this.openTimestamp;
    }
    return this;
  }

  setClose(closeValue, timestamp) {
    if (!closeValue) throw Error('closeValue is required.');
    if (!timestamp) throw Error('close timestamp is required.');

    if (!this.complete) {
      this.close = closeValue;
      this.closeTimestamp = timestamp;
    }
    return this;
  }

  setCloseTimestamp(closeTimestamp) {
    if (!closeTimestamp) throw Error('closeTimestamp is required.');

    if (!this.complete) this.closeTimestamp = closeTimestamp;
    return this;
  }

  completeCandle() {
    this.complete = true;
    return this;
  }

  newValue(value, volume, timestamp) {
    if (!value || !timestamp) return this;

    this.setClose(value, timestamp);

    if (value > this.high) {
      this.setHigh(value);
    } else if (value < this.low) {
      this.setLow(value);
    }

    this.setVolume(volume + this.volume);

    return this;
  }

  async persist() {
    try {
      await CandlePersistent.create(Object.assign({}, this, { time: this.closeTimestamp }));
    } catch (e) {
      logger.error('Error while trying to persist a candle');
      logger.error(e);
    }
    return this;
  }
}

module.exports = Candle;
