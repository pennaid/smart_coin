const round05 = (number) => {
  const int = parseInt(number.toFixed(0), 10);
  const decimal = number - int;
  if (decimal > 0 && decimal < 0.5) return int;
  return int + 0.5;
};

module.exports = { round05 };
