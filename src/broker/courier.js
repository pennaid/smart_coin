const axios = require('axios');

const { logger } = require('../helpers');

const { STRATEGY_URL, STRATEGY_AUTH_KEY } = process.env;

async function go({ candles4h, settings, createEvent }) {
  if (!candles4h || !candles4h.length) {
    logger.error('Broker :: Missing candles');
    return setTimeout(() => createEvent({ type: 'RUN_STRATEGY' }), 1000);
  }

  try {
    const data = {
      candles: candles4h,
      wmaLength: settings.wmaLength,
      emaLength: settings.emaLength,
      hullLength: settings.hullLength,
      bbMult: settings.bbMult,
      bbLength: settings.bbLength,
    };

    const response = await axios({
      // url: `${STRATEGY_URL}/strategies/v1`,
      url: 'http://172.17.0.1:8097/strategies/v2',
      method: 'POST',
      headers: { authentication: STRATEGY_AUTH_KEY },
      data,
    }).catch((e) => e.response);

    if (!response) {
      logger.error('Broker :: STRATEGY API didn\'t respond');
      return setTimeout(() => createEvent({ type: 'RUN_STRATEGY' }), 3000);
    }

    const advice = response.data;

    if (!advice.success) {
      logger.error('Broker :: STRATEGY API returned error', advice.error, advice.stack);
      return setTimeout(() => createEvent({ type: 'RUN_STRATEGY' }), 5000);
    }
    // EVENT: advice error (retry, count 3)

    return createEvent({ type: 'RECEIVED_ADVICE', advice });
  } catch (e) {
    logger.error('strategy.run :: Error running strategy', e);
    return setTimeout(() => createEvent({ type: 'RUN_STRATEGY' }), 1000);
  }
}

module.exports = { go };
