/* eslint-disable no-param-reassign */
const { redis, main } = require('../config');
const { logger, telegram, persistLog } = require('../helpers');
const Sender = require('./sender');
const courier = require('./courier');
const fundingStrategy = require('./fundingStrategy');
const bitmexWS = require('../exchanges/bitmex');
const bitmexRest = require('../exchanges/rest/bitmex');

const { client: redisClient } = redis;
const { TURNED_ON_TOKEN } = main;

const { BITMEX_PROD } = process.env;

const { model: ClientModel } = require('../models/Client');
const { model: SettingsModel } = require('../models/Settings');

const runClient = async(client, runtime) => {
  client.running = true;
return;
  try {
    const { accountId } = client;
    // let hasOpenOrders = false;
    await client.dispatch();
    console.log('\n\x1b[43mEND PROCESSING ADVICE\x1b[0m');

    // createEvent({ type: 'CLEAN_ORDERS', accountId });
    const clientOrders = runtime.orders[accountId] || [];
    clientOrders.forEach((item, index) => {
      const clean = !/new|partially|pending/i.test(item.ordStatus);
      if (clean) clientOrders.splice(index, 1);
    });
    const hasOpenOrders = clientOrders.length;

    // // has open orders
    // if (runtime.orders[accountId].length) {
    //   if (!client.openOrderPosition) { // is not tracking
    //     client.openOrderPosition = openOrders.push(1); // insert random value
    //   }
    //   // dont have open orders and is tracking
    // } else if (client.openOrderPosition) {
    //   openOrders.splice(client.openOrderPosition, 1);
    //   client.openOrderPosition = null;
    // }
    if (hasOpenOrders) return setTimeout(() => runClient(client, runtime), 5000);
    setTimeout(() => runClient(client, runtime), 5000);
    // setTimeout(() => createEvent({ type: 'RUN_CLIENTS' }), 5000);
  } catch (e) {
    logger.error('Error running sender: ', e);
  }
};

// eslint-disable-next-line
module.exports = async function (createEvent, job, runtime, notificationBot) {
  const { type } = job.data;
  const orderbookSize = 50; // Goes at least $25 deep the orderbook
  let strategyIsRunning = false;

  const setRunStrategyTimeout = (time = 5000) => {
    clearTimeout(this.strategyTimeout);
    this.strategyTimeout = setTimeout(() => createEvent({ type: 'RUN_STRATEGY' }), time);
  };

  switch (type) {
    case 'RECEIVED_ADVICE': {
      console.log('\x1b[43mRECEIVED NEW ADVICE\x1b[0m');
      const { enter } = job.data.advice;
      const { close } = job.data.advice;
      const colorEnt = enter === 'short' ? 41 : (enter ? 42 : 37); // eslint-disable-line
      const colorClo = close === 'short' ? 41 : (close ? 42 : 37); // eslint-disable-line
      console.log('\n\x1b[33m%s\x1b[0m', 'New advice',
        `Enter: \x1b[${colorEnt}m${enter}\x1b[0m`,
        `Close: \x1b[${colorClo}m${close}\x1b[0m`,
        '\n');

      // PERSIST ADVICE CHANGES
      if (runtime.advice.close !== job.data.advice.close ||
        runtime.advice.enter !== job.data.advice.enter) {
        persistLog({ enter: job.data.advice.enter, close: job.data.advice.close });
      }

      // const start = Date.now();
      runtime.advice = job.data.advice;
      if (!runtime.clients.length) return setRunStrategyTimeout(2000);
      setRunStrategyTimeout(2000);
      // console.log('Finish processing: ', Date.now() - start);
      return true;
    }

    case 'RUN_STRATEGY': {
      if (!runtime.wsConnection.bitmex) logger.error('Bitmex WS public connection missing.');

      redisClient.get('BOT_STATE', (err, state) => {
        if (state === TURNED_ON_TOKEN) {
          // const { candles4h, settings } = runtime;
          // runtime.run({ candles4h, settings, createEvent });
          const { candles: { bitmex }, settings } = runtime;
          runtime.run({ candles4h: bitmex, settings, createEvent });
          return true;
        }
      });

      setRunStrategyTimeout(5000);
      return true;
    }

    // ---------------- ORDERBOOK ---------------------

    // 0 - buy | 1 - sell, for performance
    case 'RECEIVED_INITIAL_ORDERBOOK': {
      let { received } = job.data;
      received = received || await bitmexRest.getOrderbook();

      const buys = received
        .filter((item) => item.side[0] === 'B')
        .sort((a, b) => {
          if (a.price > b.price) return -1;
          if (a.price < b.price) return 1;
          return 0;
        });
      const sells = received
        .filter((item) => item.side[0] === 'S')
        .sort((a, b) => {
          if (a.price < b.price) return -1;
          if (a.price > b.price) return 1;
          return 0;
        });
      const data = buys.concat(sells);

      data.reduce((acc, item) => {
        if (item.side[0] === 'B' && acc[0].length < orderbookSize) {
          acc[0].push(item);
        } else if (item.side[0] === 'S' && acc[1].length < orderbookSize) {
          acc[1].push(item);
        }
        return acc;
      }, runtime.orderbook); // 0 - Buy | 1 - Sell

      return true;
    }

    case 'UPDATE_ORDERBOOK': {
      const { received } = job.data;

      received.reduce((acc, item) => {
        const side = item.side[0] === 'B' ? 0 : 1;
        const index = acc[side].findIndex((b) => b && b.id === item.id);
        if (index >= 0) acc[side][index] = { ...acc[side][index], ...item };
        return acc;
      }, runtime.orderbook);

      return true;
    }

    case 'INSERT_ORDERBOOK': {
      const { received } = job.data;
      const { orderbook } = runtime;

      received.reduce((acc, item) => {
        const side = item.side[0] === 'B' ? 0 : 1;
        const sidedAcc = acc[side];
        let index = !side ? sidedAcc.findIndex((order) => order.price < item.price)
          : sidedAcc.findIndex((order) => order.price > item.price);
        // Add item anyway if orderbook is smaller than minimum size
        if (index < 0 && sidedAcc.length < orderbookSize) index = sidedAcc.length;
        if (index >= 0) sidedAcc.splice(index, 0, item);
        return acc;
      }, orderbook);

      if (orderbook[0].length > orderbookSize) orderbook[0].length = orderbookSize;
      if (orderbook[1].length > orderbookSize) orderbook[1].length = orderbookSize;


      return true;
    }

    case 'DELETE_ORDERBOOK': {
      const { received } = job.data;

      received.reduce((acc, item) => {
        const side = item.side[0] === 'B' ? 0 : 1;
        const index = acc[side].findIndex((order) => order && order.id === item.id);
        if (index >= 0) acc[side].splice(index, 1);
        return acc;
      }, runtime.orderbook);

      return true;
    }

    // ---------------- POSITIONS ---------------------

    case 'RECEIVED_INITIAL_POSITION': {
      let { received } = job.data;
      const { id } = job.data;
      const data = runtime.positions;
      received = received || await bitmexRest.getPositions({
        key: runtime.clients[id].key,
        secret: runtime.clients[id].secret,
      });

      Object.assign(data, { [id]: received });

      return true;
    }

    case 'UPDATE_POSITION': {
      const { received, id } = job.data;

      const data = runtime.positions;
      Object.assign(data[id], received);

      return true;
    }

    // ---------------- ORDERS ---------------------

    case 'RECEIVED_INITIAL_ORDERS': {
      let { received } = job.data;
      const { id } = job.data;
      received = received || await bitmexRest.getOpenOrders({
        key: runtime.clients[id].key,
        secret: runtime.clients[id].secret,
      });

      Object.assign(runtime.orders, { [id]: received });

      return true;
    }

    case 'UPDATE_ORDER': {
      const { received, id } = job.data;
      const data = runtime.orders;

      data[id] = data[id] || [];

      received.reduce((acc, order) => {
        const index = acc.findIndex((a) => a.orderID === order.orderID);
        acc[index] = { ...acc[index], ...order };
        return acc;
      }, data[id]);

      return true;
    }

    case 'INSERT_ORDER': {
      const { received, id } = job.data;
      const data = runtime.orders;
      // console.log('insert new order INIT', data);
      data[id] = data[id] || [];

      received.reduce((acc, order) => {
        acc.push(order);
        return acc;
      }, data[id]);
      // console.log('insert new order END', data);

      return true;
    }

    case 'CLEAN_ORDERS': {
      const { accountId } = job.data;
      const data = runtime.orders;
      data[accountId] = data[accountId] || [];

      data[accountId].forEach((item, index) => {
        const clean = !/new|partially|pending/i.test(item.ordStatus);
        if (clean) data[accountId].splice(index, 1);
      });

      return true;
    }

    // ---------------- MARGIN ---------------------

    case 'RECEIVED_INITIAL_MARGIN': {
      let { received } = job.data;
      const { id } = job.data;
      const data = runtime.margins;
      received = received || await bitmexRest.getMargin({
        key: runtime.clients[id].key,
        secret: runtime.clients[id].secret,
      });

      Object.assign(data, { [id]: received });

      return true;
    }

    case 'UPDATE_MARGIN': {
      const { received, id } = job.data;
      const data = runtime.margins;

      Object.assign(data[id], received);

      return true;
    }

    // ---------------- SETTERS ---------------------

    case 'SET_CLIENT': {
      const { accountId } = job.data;
      const client = await ClientModel.getPrivateClient(accountId);
      if (!client) {
        throw Error('broker.events.SET_CLIENT :: NO CLIENT INCLUDED. PLEASE ADD CLIENT.');
      }
      if (client.test && BITMEX_PROD === 'true') {
        logger.error('broker.events.SET_CLIENT :: Trying to init wrong environment client.');
        return true;
      }
      const clientSender = new Sender(client, runtime);
      runtime.clients.push(clientSender);
      createEvent({ type: 'RUN_CLIENTS' });
      logger.info(`Client \x1b[32mACTIVE\x1b[0m (${accountId}): ${client.firstname} ${client.lastname}`);
      return true;
    }

    case 'RUN_CLIENTS': {
      if (!runtime.advice.success) {
        console.log('runtime.advice', runtime.advice);
        setTimeout(() => createEvent({ type: 'RUN_CLIENTS' }), 5000);
        return logger.error('Advice missing or error returned. Skiping client.');
      }

      runtime.clients.forEach((client) => {
        if (client.running) return;
        // Wait 5 sec to initialize client to give more assurance all data is fetched
        setTimeout(() => runClient(client, runtime), 5000);
        // setTimeout(() => createEvent({ type: 'RUN_CLIENTS' }), 10000);
      });
      // has open orders
      // setTimeout(() => createEvent({ type: 'RUN_CLIENTS' }), 10000);
      // setTimeout(() => createEvent({ type: 'RUN_CLIENTS' }), 5000);
      return true;
    }

    case 'ABORT_CLIENT': {
      const { accountId } = job.data;
      const { clients } = runtime;

      const index = clients.findIndex((client) => client.accountId === accountId);
      if (index < 0) {
        logger.error(`broker.events.ABORT_CLIENT :: Client not found in Runtime (${accountId})`);
        return true;
      }
      const client = clients.splice(index, 1);
      Promise.all([
        client.handleCloseAdvice(),
        client.handleCloseAdvice(),
      ]).catch((e) => logger.error(`Failed to abort client's positions (accountId: ${accountId})`, e));

      logger.info(`Client \x1b[31mABORTED\x1b[0m (${accountId}): ${client.firstname} ${client.lastname}`);

      return true;
    }

    case 'SET_SETTINGS': {
      const settings = await SettingsModel.findByExchange({ exchange: 'BMX' });
      runtime.settings = settings;
      return true;
    }

    case 'SET_STRATEGY': {
      logger.info(`Setting Strategy: ${job.data.strategy}`);

      if (job.data.strategy === 'courier') runtime.run = courier.go;
      else if (job.data.strategy === 'fundingStrategy') runtime.run = fundingStrategy.go;

      if (!strategyIsRunning) createEvent({ type: 'RUN_STRATEGY' });
      strategyIsRunning = true;
      return true;
    }

    case 'CONNECT_PUBLIC_BITMEX': {
      bitmexWS.publicCnx({ createEvent });
      return true;
    }

    case 'CONNECT_PRIVATE_BITMEX': {
      const clients = await ClientModel.getActiveClients();
      if (!clients || !clients.length) {
        logger.error('broker.events.CONNECT_PRIVATE_BITMEX :: NO CLIENT INCLUDED. PLEASE ADD CLIENT.');
      }
      clients.forEach((client) => {
        if (client.test && BITMEX_PROD === 'true') return;
        bitmexWS.privateCnx({
          key: client.key,
          secret: client.secret,
          accountId: client.accountId,
          createEvent,
        });
      });
      return true;
    }

    case 'UPDATE_CLIENT': {
      const { accountId } = job.data;
      const clientSender = runtime.clients.find((sender) => sender.checkId(accountId));
      if (!clientSender) return true;

      const client = await ClientModel.getPrivateClient(accountId);
      if (!client) {
        logger.error(`broker.events.UPDATE_CLIENT :: Client not found ${accountId} at DB`);
      }

      clientSender.updateClient(client);
      logger.info(`Client \x1b[32mUpdated\x1b[0m (${accountId}): ${client.firstname} ${client.lastname}`);
      return true;
    }

    // ---------------- STATUS ---------------------

    case 'BITMEX_PUBLIC_CONNECTION': {
      const { connected } = job.data;
      runtime.wsConnection.bitmex = connected;
      if (!connected) {
        telegram(notificationBot).sendMessage(
          `Bitmex PUBLIC websocket ${connected ? '' : 'dis'}connected`,
          'WARNING',
        );
      }
      const msg = connected ? 'Bitmex public WS \x1b[32mCONNECTED\x1b[0m' :
        'Bitmex public WS \x1b[31mDISCONNECTED\x1b[0m';
      logger.info(msg);
      return true;
    }

    case 'BITMEX_PRIVATE_CONNECTION': {
      const { accountId, connected } = job.data;
      const client = runtime.clients[accountId];

      if (connected) {
        createEvent({ type: 'SET_CLIENT', accountId });
      } else {
        createEvent({ type: 'ABORT_CLIENT', accountId });
        const msg = `Bitmex private websocket disconnected for user ${accountId} ${client.firstname} ${client.lastname}`;
        logger.info(msg);
        telegram(notificationBot).sendMessage(msg, 'WARNING');
      }

      return true;
    }

    default: return false;
  }
};
