const { logger } = require('../helpers');
const { CronJob } = require('cron');
const fetcher = require('./fetcher');

function startFundingTimers(createEvent) {
  logger.info('Settings Funding Timers :: Time now (Europe/Dublin UTC): ', (new Date()).toUTCString());

  const setFundingStrategy = async function () {
    logger.info('Starting Funding Strategy');
    createEvent({ type: 'SET_STRATEGY', strategy: 'fundingStrategy' });
  };

  const unsetFundingStrategy = async function () {
    logger.info('Ending Funding Strategy :: Next Funding in 8 hours');
    createEvent({ type: 'SET_STRATEGY', strategy: 'courier' });
  };

  const timeZone = 'Europe/Dublin';
  const start = true;

  const times = [4, 12, 20];

  times.forEach((time) => {
    const startHour = time - 1;
    const startMinute = 20;
    const endHour = time;
    const endMinute = 10;

    // Start funding
    // eslint-disable-next-line
    new CronJob({ cronTime: `0 ${startMinute} ${startHour} * * *`,
      onTick: setFundingStrategy,
      start,
      timeZone,
    });
    // End funding
    // eslint-disable-next-line
    new CronJob({
      cronTime: `0 ${endMinute} ${endHour} * * *`,
      onTick: unsetFundingStrategy,
      start,
      timeZone,
    });

    // Check if funding should be setted now
    const startDate = new Date();
    startDate.setUTCHours(startHour);
    startDate.setUTCMinutes(startMinute);

    const endDate = new Date();
    endDate.setUTCHours(endHour);
    endDate.setUTCMinutes(endMinute);

    const nowDate = new Date();

    if (nowDate > startDate && nowDate < endDate) setFundingStrategy();
  });
}

function goContext() {
  let lastRun = Infinity;

  return async function go({ createEvent }) {
    // Run each 5 min to check funding rate again
    if (lastRun - Date.now() < 300000) return null;
    lastRun = Date.now();

    const advice = { success: true, enter: null, close: null };

    // const momentRate = deviation < 50 ? false :
    //   open <= close ? (longcondition ? true : (shortcondition ? false : true)) : true;

    const fundingRate = await fetcher.getFundingRate();
    logger.info(`Funding rate: ${fundingRate}`);
    // close short
    if (fundingRate < 0) advice.close = 'short';
    // close long
    if (fundingRate > 0) advice.close = 'long';

    return createEvent({ type: 'RECEIVED_ADVICE', advice });
  };
}

module.exports = {
  startFundingTimers,
  go: goContext(),
};
