/* eslint-disable no-param-reassign, max-len */
const { logger } = require('../helpers');
const utils = require('../helpers/utils');

const bitmexRest = require('../exchanges/rest/bitmex');

class Sender {
  // const maxContractSize = 50000;
  // const maxAssetSize = 8;
  constructor(client, runtime) {
    this.client = client;
    this.accountId = client.accountId;
    this.runtime = runtime;
    this.running = false;
    this.refreshRuntime();
  }

  refreshRuntime() {
    const { runtime, accountId } = this;
    if (!runtime.orders[accountId]) runtime.orders[accountId] = [];
    if (!runtime.positions[accountId]) runtime.positions[accountId] = [];
    if (!runtime.margins[accountId]) runtime.margins[accountId] = {};

    this.orderbook = runtime.orderbook;
    this.settings = runtime.settings;
    this.orders = runtime.orders[accountId];
    this.position = runtime.positions[accountId];
    this.margin = runtime.margins[accountId];
  }

  async dispatch() {
    // this.running = true;
    const { enter, close } = this.runtime.advice;
    console.log('\n\x1b[37;46m%s\x1b[0m', 'STARTING with interval', Date.now() - (this.lastStart || Date.now()));
    this.lastStart = Date.now();
    console.log('\n\x1b[36m%s\x1b[0m', 'Remaining requests', this.remainingRequests);

    if (!close && !enter) {
      // this.running = false;
      return this.checkClosingOrders();
      // return this.closeAllOrders();
    }
    if (close === enter) {
      // this.running = false;
      return Promise.resolve(logger.error('Advicer returned CLOSE = ENTER.'));
    }

    if (this.spamTime && this.spamTime - Date.now() < 60000) {
      // this.running = false;
      return null;
    }
    this.spamTime = null;

    try {
      const handled = await (enter ? this.handleEnterAdvice() : this.handleCloseAdvice());
      // this.running = false;
      return handled;
    } catch (e) {
      // this.running = false;
      return logger.error('Client Sender dispatch() :: ', e);
    }
  }

  handleCloseAdvice() {
    console.log('\x1b[43m%s\x1b[0m\n', 'Handle Close');
    const { close: adviceClose } = this.runtime.advice;

    const { currentQty: positionSize } = this.position;
    // WARNING! ordersSize SHOULD NOT be used anywhere for REAL order size
    const ordersSize = this.orders.reduce((acc, order) => {
      const status = /new|partially|pending/i.test(order.ordStatus);
      const side = order.side[0] === 'B' ? 1 : -1;
      return status ?
        acc + ((order.orderQty + order.simpleOrderQty) * side)
        : acc;
    }, 0);

    if (adviceClose === 'long') {
      // if close long, send -amount for sell, otherwise +amount for buy
      this.mult = -1;
      // if close long, orderbook side prices are 'sell' (1), otherwise 'buy' (0)
      this.side = 1;
    } else {
      this.mult = 1;
      this.side = 0;
    }
    const keepPosition = positionSize * this.mult > 0;
    const keepOrders = ordersSize * this.mult > 0;

    console.log('Position Size:', positionSize);
    console.log('Orders Size:', ordersSize);

    if (positionSize) { // has position
      if (!keepPosition) { // should close position
        // no order, send new for closing
        if (!ordersSize) return this.handleOrders({ closeAmount: -positionSize });
        // orders agree with advice, just check if they close
        if (keepOrders) return this.checkClosingOrders();
        // orders dont agree, update
        return this.handleOrders({ closeAmount: -positionSize, cancelOrders: true });
      }
    } // dont have position
    // dont have orders too, just skip
    if (!ordersSize) return null;
    // has orders, cancel all since there is no enter advice
    return this.handleOrders({ cancelOrders: true });
  }

  handleEnterAdvice() {
    console.log('\x1b[42m%s\x1b[0m\n', 'Handle Enter');
    // If should wait remaining requests recover, dont enter new positions
    if (Date.now() - this.waitRemaining < 30000) {
      if (this.runtime.advice.close) return this.handleCloseAdvice();
      return null;
    }

    const { enter: adviceEnter } = this.runtime.advice;

    const { currentQty: positionSize = 0 } = this.position;
    // WARNING! ordersSize SHOULD NOT be used anywhere for REAL order size
    const ordersSize = this.orders.reduce((acc, order) => {
      const status = /new|partially|pending/i.test(order.ordStatus);
      return status ?
        (acc + ((order.orderQty + order.simpleOrderQty) * (order.side[0] === 'B' ? 1 : -1)))
        : acc;
    }, 0);

    if (adviceEnter === 'long') {
      // if enter long, send +amount
      this.mult = 1;
      // if enter long, orderbook side prices are 'buy' (0), otherwise 'sell' (1)
      this.side = 0;
    } else {
      this.mult = -1;
      this.side = 1;
    }
    const keepPosition = positionSize * this.mult > 0;
    const keepOrders = ordersSize * this.mult > 0;

    const {
      amount,
      riskValue: marginUsed,
      initMargin: ordersMargin,
      // maintMargin: positionMargin,
    } = this.margin;
    // Selected amount to use by bot
    const budget = Math.floor(amount * this.client.budget);
    // Available budget considering used margin
    const budgetAvailable = Math.abs(budget - marginUsed);
    // Margin available to be used (considering leverage)
    const budgetLeveraged = budget * (this.position.leverage || 1) * this.mult;
    const availableLeveraged = budgetAvailable * (this.position.leverage || 1) * this.mult;
    const availWithOrders = (budgetAvailable + ordersMargin) * (this.position.leverage || 1) * this.mult;

    console.log('budget', budget);
    console.log('budgetAvailable', budgetAvailable);
    console.log('budgetLeveraged', budgetLeveraged);
    console.log('availableLeveraged', availableLeveraged);
    console.log('availWithOrders', availWithOrders);

    if (positionSize) { // has position?
      if (keepPosition) { // position = open
        // should keep orders
        if (!ordersSize || keepOrders) return this.checkMargin();
        // should change orders
        return this.handleOrders({ assetAmount: availWithOrders, cancelOrders: true });
      } // position != open
      // dont have orders, send new order
      if (!ordersSize) return this.handleOrders({ assetAmount: budgetLeveraged, closeAmount: positionSize * -1 });
      // orders agree with advice, just check margin
      if (keepOrders) return this.checkMargin();
      // orders dont agree, close position, orders and update
      return this.handleOrders({ assetAmount: budgetLeveraged, closeAmount: positionSize * -1, cancelOrders: true });
    } // dont have position
    // dont have orders, send new
    if (!ordersSize) return this.handleOrders({ assetAmount: availableLeveraged });
    // orders agree with advice, just check margin
    if (keepOrders) return this.checkMargin();
    // orders dont agree, update
    return this.handleOrders({ assetAmount: availableLeveraged, cancelOrders: true });
  }

  handleOrders({ amount, assetAmount, closeAmount, cancelOrders }) {
    console.log('\x1b[32;47m%s\x1b[0m\n', 'Handle Orders');
    if ((!this.side && this.side !== 0) || (!this.mult && this.mult !== 0)) return null;
    const closePromise = cancelOrders ? bitmexRest.cancelAllOrders({
      key: this.client.key,
      secret: this.client.secret,
    }) : null;
    console.log('handle orders arguments', arguments);
    if (!amount && !closeAmount && !assetAmount) return closePromise;

    const assetXBT = assetAmount / 100000000;

    // CAUTION! INDEX -> 0: contract amount, 1: close contract amount, 2: asset amount
    const pyramid = [amount, closeAmount, assetXBT].reduce((acc, size, index) => {
      if (!size) return acc;

      // const compareMin = index === 2 ? this.settings.minAssetSize : this.settings.minContractSize;
      const sizeAbs = size * this.mult; // always positive
      // if (sizeAbs <= compareMin) return acc;

      const compareMax = index === 2 ? this.settings.maxAssetSize : this.settings.maxContractSize;

      if (sizeAbs <= compareMax) {
        acc.push([size, index]);
      } else {
        const parts = Math.floor(size / compareMax * this.mult);
        for (let i = 1; i <= parts; i++) { // eslint-disable-line no-plusplus
          acc.push([compareMax * this.mult, index]);
        }
        acc.push([size - (parts * compareMax * this.mult), index]);
      }
      return acc;
    }, []);

    console.log('pyramid', pyramid);

    return this.sendOrders(pyramid);
  }

  async sendOrders(orders) {
    console.log('\x1b[44m%s\x1b[0m\n', 'Send Orders');
    if ((!this.side && this.side !== 0) || (!this.mult && this.mult !== 0) || !orders.length) return null;
    const bulk = bitmexRest.createBulkOrder({ key: this.client.key, secret: this.client.secret });
    const price = this.calculatePrice();

    console.log('\x1b[32m%s\x1b[0m', 'Primary Price:', price, '\n');
    if (!price) console.log('ORDERBOOK', this.runtime.orderbook);

    const bestPrice = this.orderbook[this.side][0].price;
    const bestOppositePrice = this.orderbook[this.side ? 1 : 0][0].price;

    let countClose = 0;
    let countContract = 0;
    let countAsset = 0;

    orders.forEach((block) => {
      let calcPrice;

      if (block[1] === 1) {
        calcPrice = price + (this.side ? 0.5 : -0.5) * countClose; // buy: -0.5, sell: +0.5
        countClose += 1;
      } else if (block[1] === 2) {
        calcPrice = price + (this.side ? 0.5 : -0.5) * countAsset;
        countAsset += 1;
      } else {
        calcPrice = price + (this.side ? 0.5 : -0.5) * countContract;
        countContract += 1;
      }

      // If new price will cancel, set price to the best price (first position on orderbook)
      if ((this.side && calcPrice <= bestOppositePrice)
        || (!this.side && calcPrice >= bestOppositePrice)) calcPrice = bestPrice;

      if (block[1] === 1) {
        console.log(bitmexRest.bulk);
        bitmexRest.sendLimitOrder({
          contractAmount: block[0],
          price: calcPrice,
          instructions: 'ParticipateDoNotInitiate,Close',
        });
        return;
      }

      bulk.sendLimitOrder({
        [block[1] === 2 ? 'assetAmount' : 'contractAmount']: block[0],
        price: calcPrice,
        instructions: 'ParticipateDoNotInitiate',
      });
    });

    // const result = await bulk.sendBulkOrder();
    // const lastResponse = await this.handleFailedOrders(result);
    // if (!lastResponse) return null;
    // this.remainingRequests = lastResponse.headers['x-ratelimit-remaining'];
    // return null;
    return bulk && this.sendBulk(bulk);
  }

  async sendBulk(bulk) {
    const result = await bulk.sendBulkOrder();
    if (!result) return logger.error('Bulk response is null :: Sender.sendBulk()');

    this.remainingRequests = result && result.headers['x-ratelimit-remaining'];
    if (this.remainingRequests && this.remainingRequests < 30) this.waitRemaining = Date.now();
    else this.waitRemaining = 0;

    if (result.data.message === 'Spam') {
      this.spamTime = Date.now();
    }
    return result;
  }

  calculatePrice() {
    if (!this.side && this.side !== 0) return null;
    const sided = this.runtime.orderbook[this.side];

    const avg = sided.reduce((acc, order, index) => {
      if (!order) return acc;
      if (acc[1] < 20000 || index < 2) {
        acc[0] += order.price * order.size;
        acc[1] += order.size;
      }
      return acc;
    }, [0, 0]); // 0: price, 1: contract amount

    // console.log('\n\x1b[32m%s\x1b[0m', 'Calculated Price', utils.round05((avg[0] / avg[1])) + (this.side ? -0.5 : 0.5));
    // console.log('this.side', this.side);
    // console.log('avgPrice:', avg[0]);
    // console.log('avgSize:', avg[1]);
    // console.log('division', avg[0] / avg[1]);

    // return utils.round05((avg[0] / avg[1])) + (this.side ? -0.5 : 0.5); // buy: +0.5, sell: -0.5
    return utils.round05((avg[0] / avg[1])); // buy: +0.5, sell: -0.5
  }

  handleFailedOrders(result) {
    return null;
    console.log('\x1b[31;47m%s\x1b[0m\n', 'Handle Failed Orders');
    if (!result) return null;
    console.log('DATA', result.data);
    if (result.data.message === 'Spam') {
      this.spamTime = Date.now();
      return null;
    }
    const canceleds = result.data.reduce((acc, order) => {
      // eslint-disable-next-line
      const opt = /ReduceOnly/.test(order.execInst) ? 1 : (order.simpleOrderQty ? 2 : 0); // eslint-disable-line
      if (order.ordStatus === 'Canceled') {
        const mult = order.side[0] === 'B' ? 1 : -1;
        const amount = order[order.orderQty ? 'orderQty' : 'simpleOrderQty'] * mult;
        acc.push([amount, opt]);
        // Remove canceled
        const index = this.orders.findIndex((o) => o.orderID === order.orderID);
        this.orders.splice(index, 1);
      }
      return acc;
    }, []);
    if (!canceleds.length) return result;
    return this.sendOrders(canceleds);
  }

  closeAllOrders() {
    if (!this.orders.length) return null;
    return bitmexRest.cancelAllOrders({ key: this.client.key, secret: this.client.secret });
  }

  async checkOrderRank() {
    console.log('\n\x1b[30;47m%s\x1b[0m', 'Check order rank');
    if ((!this.mult && this.mult !== 0) || (!this.side && this.side !== 0) || !this.orders.length) return null;
    const orderBulk = bitmexRest.createBulkOrder({ key: this.client.key, secret: this.client.secret });
    const price = this.calculatePrice();
    let count = 0;

    this.orders.forEach((order) => {
      const bestPrice = this.orderbook[this.side][0].price;
      const bestOppositePrice = this.orderbook[this.side ? 1 : 0][0].price;
      console.log('\x1b[32m%s\x1b[0m', 'Best Price', bestPrice);
      console.log('\x1b[34m%s\x1b[0m', 'Order Price', order.price);
      if (order.price === bestPrice) return;

      const status = /new|partially|pending/i.test(order.ordStatus);
      if (!status) return;

      let calcPrice = price + (this.side ? 0.5 : -0.5) * count; // buy: -0.5, sell: +0.5

      // If new price will cancel, set price to the best price (first position on orderbook)
      if ((this.side && calcPrice <= bestOppositePrice)
        || (!this.side && calcPrice >= bestOppositePrice)) calcPrice = bestPrice;

      const orderMult = order.side[0] === 'B' ? 1 : -1;
      const amount = order[order.orderQty ? 'orderQty' : 'simpleOrderQty'] * orderMult;
      count += 1;
      console.log('\x1b[33m%s\x1b[0m', 'Calc Price', calcPrice);
      if (calcPrice === order.price) return;

      orderBulk.updateOrder({
        orderId: order.orderID,
        [order.orderQty ? 'contractAmount' : 'assetAmount']: amount,
        price,
        instructions: order.execInst,
      });
    });

    if (!orderBulk.bulk.length) return null;
    // const result = await orderBulk.sendBulkOrder();
    // return this.handleFailedOrders(result);
    return this.sendBulk(orderBulk);
  }

  checkMargin() {
    console.log('\n\x1b[30;47m%s\x1b[0m', 'Check margin');
    if ((!this.mult && this.mult !== 0) || (!this.side && this.side !== 0)) return null;
    const { amount, riskValue: marginUsed } = this.margin;
    // Selected amount to use by bot
    const budgetLeveraged = Math.floor(amount * this.client.budget) * (this.position.leverage || 1);
    const budgetAvailable = budgetLeveraged - marginUsed;
    console.log('budgetLeveraged', budgetLeveraged);
    console.log('marginUsed', marginUsed);
    console.log('budgetAvailable', budgetAvailable);
    // Margin available to be used (considering leverage)
    const budgetToUse = budgetAvailable > 0 ? budgetAvailable : 0;
    // console.log('budgetToUse', budgetToUse);
    // console.log('budgetAvailable', budgetAvailable);
    // console.log('budget', budget);
    // console.log('marginUsed', marginUsed);
    // console.log('div', budgetToUse / (this.settings.minAssetSize * 100000000));
    if (budgetToUse / (this.settings.minAssetSize * 100000000) <= 1) return this.checkOrderRank();
    return this.handleOrders({ assetAmount: budgetToUse * this.mult });
  }

  checkClosingOrders() {
    console.log('\n\x1b[30;47m%s\x1b[0m', 'Check closing orders');
    const ordersSize = this.orders.reduce(
      (acc, order) => acc + (order.orderQty * (order.side[0] === 'B' ? 1 : -1)),
      0,
    );
    if (this.position.currentQty === -ordersSize) return this.checkOrderRank();
    return this.handleOrders({ amount: -(this.position.currentQty + ordersSize) });
  }

  /* **************** CLASS INTERNAL METHODS *************** */

  checkId(accountId) {
    return this.accountId === accountId;
  }

  updateClient(client) {
    this.client = client;

  }
}

module.exports = Sender;
