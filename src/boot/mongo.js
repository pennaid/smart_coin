const mongoose = require('mongoose');

mongoose.Promise = require('bluebird');
require('../models/Settings');

const { mongo: mongoConfig } = require('../config');
const { logger } = require('../helpers');

mongoose.Promise = Promise;

module.exports = () => new Promise((fulfill, reject) => {
  if (mongoose.connection.readyState === 1) {
    return fulfill(mongoose.connection);
  }

  return mongoose.connect(`${mongoConfig.URI}/${mongoConfig.DATABASE}`, { useMongoClient: true }, (err) => {
    if (err) return reject();
    logger.info('app.boot.mongo :: Database connected successfully.');
    return fulfill(mongoose.connection);
  });
});
