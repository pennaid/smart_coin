const _ = require('lodash');
const { logger, Candle } = require('../helpers');

class Runtime {
  constructor({ notificationBot, granularity = 460800000 } = {}) {
    this.clients = [];
    this.orderbook = [[], []]; // 0 - Buy | 1 - Sell
    this.positions = {};
    this.margins = {};
    this.orders = {};
    this.wsConnection = {};
    this.notificationBot = notificationBot;

    this.advice = {};
    this.settings = {};

    // candles
    this.candles = {}; // by exchange
    this.masterCandles = [];

    this.run = () => {};

    // Handling new candles
    this.granularity = granularity;
    this.lastCandleClose = null;
  }

  addCandles(candles = [], exchange) {
    if (!exchange) throw new Error('exchange is required.');

    this.candles[exchange] = candles;

    const { length } = candles;
    if (length > 1) {
      this.lastCandleClose = Math.floor(candles[length - 2].closeTimestamp);
    }

    logger.info(`Stored ${length} candles from ${exchange}.`);

    return this;
  }

  addTrade({ timestamp, price, amount }, exchange) {
    const { lastCandleClose, granularity } = this;
    const last = this.candles[exchange].length - 1;

    if (lastCandleClose + granularity < timestamp) {
      this.lastCandleClose = timestamp;
      this.candles[exchange][last].completeCandle();
      this.candles[exchange].shift();
      const newCandle =
        new Candle({
          high: price,
          low: price,
          open: price,
          openTimestamp: timestamp,
          volume: amount,
        });
      this.candles[exchange].push(newCandle);
    }

    this.candles[exchange][last].newValue(price, amount, timestamp);
  }

  calculateMasterCandle() {
    const { candles } = this;
    const exchanges = Object.keys(candles);

    const candleVolumes = exchanges.map((exchange) => {
      const exchangeCandles = candles[exchange];

      return exchangeCandles.reduce((sum, { volume }) => sum + volume, 0);
    });
    const sumOfVolumes = candleVolumes.reduce((sum, volume) => sum + volume, 0);
    const portions = candleVolumes.map((volume) => volume / sumOfVolumes);

    const length = _.min(exchanges.map((exchange) => candles[exchange].length));

    const masterCandles = new Array(length).fill(0);

    this.masterCandles = masterCandles.map((__, i) => {
      const candle = new Candle();

      exchanges.forEach((exchange, exi) => {
        const exchangeCandles = candles[exchange];
        const exchangeLength = exchangeCandles.length;
        const exchangeCandle = exchangeCandles[exchangeLength - length + i];
        const { high, volume, close, open, low } = candle;

        const {
          high: exHigh, volume: exVolume, close: exClose,
          open: exOpen, low: exLow, closeTimestamp,
        } = exchangeCandle;

        candle
          .setClose(close + (exClose * portions[exi]), closeTimestamp)
          .setLow(low + (exLow * portions[exi]))
          .setHigh(high + (exHigh * portions[exi]))
          .setOpen(open + (exOpen * portions[exi]))
          .setVolume(volume + (exVolume * portions[exi]));
      });

      return candle;
    });

    return this;
  }
}

module.exports = Runtime;
