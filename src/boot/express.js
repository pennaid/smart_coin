const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const tradeRoutes = require('../routes/trades');
const clientsRoutes = require('../routes/clients');
const userRoutes = require('../routes/users');
const dataRoutes = require('../routes/data');
const { logger } = require('../helpers');


module.exports = (runtime, mqHelper) => {
  const app = express();
  app.use(cors());
  app.use(bodyParser.json()); // application/json
  app.use(bodyParser.urlencoded({ extended: true })); // application/x-www-form-urlencoded

  app.use('/trades', tradeRoutes);
  app.use('/clients', clientsRoutes(mqHelper));
  app.use('/users', userRoutes);
  app.use('/data', dataRoutes(runtime));

  logger.info('app.boot.express :: Routes initialized.');

  return app;
};
