const BeeQueue = require('bee-queue');

const { redis } = require('../config');
const { logger } = require('../helpers');

module.exports = () => {
  const queue = new BeeQueue('smartcoin-mq', { redis: redis.URI });
  if (queue) logger.info('app.boot.mq :: Message Queue booted succesfully.');
  return queue;
};
