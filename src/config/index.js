const redis = require('./redis');
const mongo = require('./mongo');
const main = require('./main');

module.exports = {
  redis,
  mongo,
  main,
};
