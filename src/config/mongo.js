module.exports = {
  URI: process.env.MONGODB_URI,
  DATABASE: process.env.MONGODB_DATABASE,
};
