const redis = require('redis');

const url = process.env.REDIS_URI;
const client = redis.createClient({ url });

client.set('POSITIONS', '{}');
client.set('ORDERS', '{}');

module.exports = { URI: url, client };
