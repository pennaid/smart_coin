const fs = require('fs');
const path = require('path');

const Bitfinex = require('../services/bitfinex');

Bitfinex.getCandles(128, { timeframe: '1h', multiplier: 4 })
  .then((candles) => {
    fs.writeFileSync(path.resolve(__dirname, 'tests', 'candlesBTX.json'), JSON.stringify(candles, null, 2));
  });
