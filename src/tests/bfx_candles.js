const Candle = require('../helpers/candle');
const _ = require('lodash');
const candles = require('./candlesBFX.json');

module.exports = (multiplier = 4) => (
  candles.reduce((result, candle, index) => {
    const ind = Math.floor((index / multiplier) || 0);
    const newResult = result;
    if (!newResult[ind]) newResult[ind] = [];
    newResult[ind].push(candle);
    return newResult;
  }, [])
    .map((candle) => {
      const time = candle[0][0];

      const low = Math.abs(_.min(candle.map((c) => c[4])));
      const high = Math.abs(_.max(candle.map((c) => c[3])));
      const open = Math.abs(candle[0][1]);
      const close = Math.abs(candle[candle.length - 1][2]);
      const volume = Math.abs(candle.reduce((acc, c) => acc + c[5], 0));

      const CandleInstance = new Candle();

      return CandleInstance
        .setClose(close, time)
        .setLow(low)
        .setHigh(high)
        .setOpen(open)
        .setVolume(volume);
    })
);
