const { Router } = require('express');
const User = require('../models/User');

const router = Router();

router.post('/auth', async(req, res) => {
  const { email, password } = req.body;

  if (!email && !password) return res.sendStatus(400);

  const user = await User.findOne({ email });

  if (!user) return res.sendStatus(404);

  return user.comparePassword(password, (err, match) => {
    if (err) return res.sendStatus(401);
    if (!match) return res.sendStatus(401);
    return res.sendStatus(200);
  });
});

module.exports = router;
