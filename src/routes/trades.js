const { Router } = require('express');
const { model: Trade } = require('../models/Trade');

const router = Router();

router.get('/', async(req, res) => {
  const { exchange, limit } = req.query;
  const query = {};

  if (exchange) query.exchange = exchange;

  const trades = await Trade.find(query).sort('-1').limit(limit || 50);

  res.status(200).send(trades);
});

router.get('/candles', async(req, res) => {
  const { exchange, duration, numOfCandles } = req.query;

  const candles = await Trade.getCandles(exchange, duration, numOfCandles);

  res.status(200).send(candles);
});

module.exports = router;
