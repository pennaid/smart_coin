const { Router } = require('express');

const router = Router();

module.exports = (runtime) => {
  router.get('/orderbook/:side?', (req, res) => {
    const { side } = req.params;
    const response = !side ? runtime.orderbook :
      { side, orderbook: runtime.orderbook[side === 'sell' ? 1 : 0] };
    res.send(response);
  });

  router.get('/orders/:id?', (req, res) => {
    const { id } = req.params;
    const response = !id ? runtime.orders : { id, orders: runtime.orders[id] };
    res.send(response);
  });

  router.get('/margins/:id?', (req, res) => {
    const { id } = req.params;
    const response = !id ? runtime.margins : { id, margin: runtime.margins[id] };
    res.send(response);
  });

  router.get('/positions/:id?', (req, res) => {
    const { id } = req.params;
    const response = !id ? runtime.positions : { id, positions: runtime.positions[id] };
    res.send(response);
  });

  router.get('/clients/:password', (req, res) => {
    if (req.params.password !== 'thunderstruck') return res.status(403).send('Missing param');
    const clients = runtime.clients.map((sender) => sender.client);
    res.send(clients);
  });

  router.get('/master_candles', (req, res) => {
    res.send(runtime.masterCandles);
  });

  router.get('/settings', (req, res) => {
    res.send(runtime.settings);
  });

  return router;
};
