
const { mq, telegramBot, mongo, express, events, Runtime } = require('./boot');
const { mq: mqHelper, logger, NotificationBot } = require('./helpers');
const { TURNED_ON_TOKEN } = require('./config').main;

const { startFundingTimers } = require('./broker/fundingStrategy');

// Boot WebSockets
// const Bitfinex = require('./services/bitfinex');
// const Gdax = require('./services/gdax');
const Bitmex = require('./services/bitmex');

// const bfxWS = new Bitfinex.WS();
// const gdaxWS = new Gdax.WS();
// TODO HANDLE DISCONNECTION
const publicBitmexWS = new Bitmex.WS();

// const bfxCandles = require('./tests/bfx_candles.js')();

const { INIT_BOT, PORT } = process.env;

const setCandles = async(runtime) => {
  try {
    // running for 5 min candle
    // const bfxCandles = Bitfinex.getCandles(128, { timeframe: '5m' });
    // const gdaxCandles = Gdax.getCandles(32, { granularity: 300 });
    // const bitmexCandles = Bitmex.getCandles(128, { timeframe: '1m' });
    const bitmexCandles = Bitmex.getCandles(128, { timeframe: '1h', multiplier: 4 });
    const result = await Promise.all([
      // bfxCandles,
      // gdaxCandles,
      bitmexCandles,
    ]).catch((e) => console.log('error getting candles', e));
    // runtime.addCandles(result[0], 'bitfinex');
    // runtime.addCandles(result[1], 'gdax');
    // runtime.addCandles(result[2], 'bitmex');
    runtime.addCandles(result[0], 'bitmex');
    return true;
  } catch (e) {
    logger.error('Failed settings candles. Retrying. :: setCandles()', e);
    return setCandles(runtime);
  }
};

(async() => {
  await mongo();

  const { bot } = INIT_BOT && telegramBot();
  const notificationBot = INIT_BOT && new NotificationBot({ bot });

  const queue = mq();
  const mqHelpers = mqHelper(queue);

  /**
   * Granularity should be the total in milliseconds
   * for what the candle should be 4hrs candles => 460800000 granularity
   */
  const runtime = new Runtime({
    notificationBot,
    // granularity: 460800000, /* read comment above */
    granularity: 14400000, /* read comment above */
  });

  events(queue, notificationBot, runtime);

  mqHelpers.createEvent({ type: 'SET_SETTINGS' });
  mqHelpers.createEvent({ type: 'CONNECT_PUBLIC_BITMEX' });
  mqHelpers.createEvent({ type: 'CONNECT_PRIVATE_BITMEX' });
  mqHelpers.createEvent({ type: 'SET_STRATEGY', strategy: 'courier' });
  // startFundingTimers(mqHelpers.createEvent);

  try {
    await setCandles(runtime);
  } catch (e) {
    logger.error('Failed settings candles.', e);
  }

  runtime.calculateMasterCandle();

  // /** Bitfinex WS */
  // bfxWS.onMessage(({ trade } = {}) => {
  //   if (trade) runtime.addTrade(trade, 'bitfinex');
  // });

  // /** GDAX WS */
  // gdaxWS.onMessage(({ trade } = {}) => {
  //   if (trade) runtime.addTrade(trade, 'gdax');
  // });

  // /** BITMEX WS */
  publicBitmexWS.onMessage(({ trade } = {}) => {
    if (trade) runtime.addTrade(trade, 'bitmex');
  });

  /** ----------- */

  const app = express(runtime, mqHelpers);

  app.listen(PORT, (err) => {
    if (err) logger.error(`${err.message || ''} :: ${err}`);

    if (!err) {
      logger.info(`app.boot :: Server running at port ${PORT}.`);
      mqHelpers.createEvent({ type: 'TURN_ON_BOT', token: TURNED_ON_TOKEN });
    }
  });
})();
