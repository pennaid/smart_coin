/* eslint-disable camelcase */
const _ = require('lodash');
const axios = require('axios');
const WebSocket = require('ws');
const moment = require('moment');

const { Candle, logger } = require('../helpers');

const { BITMEX_PROD } = process.env;

const WS_URL = BITMEX_PROD === 'true'
  ? 'wss://www.bitmex.com/realtime'
  : 'wss://testnet.bitmex.com/realtime';
const API_URL = BITMEX_PROD === 'true'
  ? 'https://www.bitmex.com/api/v1/'
  : 'https://testnet.bitmex.com/api/v1/';
const PAIR = 'XBTUSD';

const PublicRequest = axios.create({ baseURL: API_URL });

const getCandles = async(num = 128, { timeframe, multiplier = 1 } = {}) => {
  let realTimeframe = timeframe;
  const timeframeTranslationTable = {
    '1m': 60,
    '5m': 180,
    '1h': 3600,
    '1d': 86400,
  };

  let timeframeNum = timeframeTranslationTable[timeframe];
  if (!timeframeNum) {
    timeframeNum = 3600;
    realTimeframe = '1h';
  }

  const refDate = moment()
    .subtract(num * timeframeNum, 'seconds');
  refDate
    .set('hour', (Math.floor(refDate.get('hour') / 4) * 4) - 4)
    .set('minute', 0)
    .set('seconds', 0)
    .set('milliseconds', 0);

  const endDate = moment().toDate();
  const startDate = refDate.toDate();

  const query = {
    method: 'GET',
    url: '/trade/bucketed',
    params: {
      // startTime: startDate,
      // endTime: endDate,
      partial: true,
      binSize: realTimeframe,
      reverse: true,
      count: 400,
      symbol: PAIR,
    },
    timeout: 5000,
  };

  try {
    const candles = (await PublicRequest(query)).data;

    const reducedCandles = candles.reverse().reduce((result, candle, index) => {
      const ind = Math.floor((index / multiplier) || 0);
      const newResult = result;
      if (!newResult[ind]) newResult[ind] = [];
      newResult[ind].push(candle);
      return newResult;
    }, []).map((candle) => {
      const time = new Date(candle[0].timestamp).getTime();

      const low = Math.abs(_.min(candle.map((c) => c.low)));
      const high = Math.abs(_.max(candle.map((c) => c.high)));
      const open = Math.abs(candle[0].open);
      const close = Math.abs(candle[candle.length - 1].close);
      const volume = Math.abs(candle.reduce((acc, c) => acc + c.homeNotional, 0));

      const CandleInstance = new Candle();

      return CandleInstance
        .setClose(close, time)
        .setLow(low)
        .setHigh(high)
        .setOpen(open)
        // .setVolume(volume);
        .setVolume(10);
    });

    return reducedCandles.slice(reducedCandles.length - (num / multiplier), reducedCandles.length);
  } catch (e) {
    logger.error('Bitmex candles timeout (3000ms). Retrying.\n', e);
    return getCandles.apply(this, arguments);
  }
};

/** WebSocket */

class WS {
  constructor() {
    const bitmexSocket = new WebSocket(WS_URL);
    this.webSocket = bitmexSocket;
    this.onOpen();
    this.opened = false;
  }

  onMessage(fun = () => {}) {
    const self = this;
    self.webSocket.on('message', (message) => {
      const parsed = JSON.parse(message);

      const { data, table } = parsed;

      if (table === 'trade' && data && Array.isArray(data)) {
        data.forEach((rawTrade) => {
          const { timestamp, price: tradePrice, size, trdMatchID } = rawTrade;
          if (!timestamp || !tradePrice || !size || !trdMatchID) return;
          const id = trdMatchID;
          const time = new Date(timestamp).getTime();
          const price = Math.abs(tradePrice);
          const amount = Math.abs(size);
          const trade = {
            id,
            timestamp: time,
            amount,
            price,
          };

          fun({ trade });
        });
      }


      fun({ trade: parsed });
    });
    return self;
  }

  onOpen(fun = () => {}) {
    const self = this;
    self.webSocket.on('open', (open) => {
      self.opened = true;
      self.subscribeTrades();
      fun(open);
    });
    return self;
  }

  subscribeTrades() {
    const self = this;
    if (!self.opened) return self;
    const event = JSON.stringify({
      op: 'subscribe',
      args: [`trade:${PAIR}`],
    });
    self.webSocket.send(event);
    return self;
  }
}

module.exports = {
  /** Public */
  getCandles,
  /** WebSocket */
  WS,
};
