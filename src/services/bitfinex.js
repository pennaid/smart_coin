const _ = require('lodash');
const axios = require('axios');
const WebSocket = require('ws');
const moment = require('moment');

const { Candle, logger } = require('../helpers');

const WS_URL_V2 = 'wss://api.bitfinex.com/ws/2';
const API_URL_V2 = 'https://api.bitfinex.com/v2/';
const PAIR = 'BTCUSD';

const PublicRequest = axios.create({ baseURL: API_URL_V2 });

const getCandles = async(num = 128, { timeframe = '1h', multiplier = 4 } = {}) => {
  let realTimeframe = timeframe;
  const timeframeTranslationTable = {
    '1m': 60,
    '5m': 180,
    '15m': 540,
    '30m': 1080,
    '1h': 3600,
    '3h': 10800,
    '6h': 21600,
    '12h': 43200,
    '1D': 86400,
    '7D': 604800,
    '14D': 1209600,
    '1M': 2419200,
  };

  let timeframeNum = timeframeTranslationTable[timeframe];
  if (!timeframeNum) {
    timeframeNum = 1080;
    realTimeframe = '30m';
    // throw new Error('Invalid time frame for bitfinex')
  }

  const refDate = moment()
    .subtract(num * timeframeNum, 'seconds');
  refDate
    .set('hour', (Math.floor(refDate.get('hour') / 4) * 4) - 4)
    .set('minute', 0)
    .set('seconds', 0)
    .set('milliseconds', 0);

  const query = {
    method: 'GET',
    url: `candles/trade:${realTimeframe}:t${PAIR}/hist`,
    params: { limit: 999, sort: 1, start: refDate.toDate().getTime() },
    timeout: 5000,
  };

  try {
    const candles = (await PublicRequest(query)).data;

    const reducedCandles = candles.reduce((result, candle, index) => {
      const ind = Math.floor((index / multiplier) || 0);
      const newResult = result;
      if (!newResult[ind]) newResult[ind] = [];
      newResult[ind].push(candle);
      return newResult;
    }, []).map((candle) => {
      const time = candle[0][0];

      const low = Math.abs(_.min(candle.map((c) => c[4])));
      const high = Math.abs(_.max(candle.map((c) => c[3])));
      const open = Math.abs(candle[0][1]);
      const close = Math.abs(candle[candle.length - 1][2]);
      const volume = Math.abs(candle.reduce((acc, c) => acc + c[5], 0));

      const CandleInstance = new Candle();

      return CandleInstance
        .setClose(close, time)
        .setLow(low)
        .setHigh(high)
        .setOpen(open)
        .setVolume(volume);
    });

    return reducedCandles.slice(reducedCandles.length - (num / multiplier), reducedCandles.length);
  } catch (e) {
    logger.error('Bitfinex candles timeout (3000ms). Retrying.\n', e.name);
    return getCandles.apply(this, arguments);
  }
};

/** WebSocket */

class WS {
  constructor() {
    const bitfinexSocket = new WebSocket(WS_URL_V2);
    this.webSocket = bitfinexSocket;
    this.onOpen();
    this.opened = false;
  }

  ping() {
    const self = this;
    if (!self.opened) return self;
    const event = JSON.stringify({ event: 'ping' });
    self.webSocket.send(event);
    return self;
  }

  onMessage(fun = () => {}) {
    const self = this;
    self.webSocket.on('message', (message) => {
      const parsed = JSON.parse(message);

      if (!Array.isArray(parsed)) return;
      const type = parsed[1];
      const values = parsed[2];

      if (type === 'te') {
        if ((!values) || !Array.isArray(values)) return;
        const id = values[0];
        const timestamp = values[1];
        const amount = Math.abs(values[2]);
        const price = Math.abs(values[3]);
        const trade = {
          id,
          timestamp,
          amount,
          price,
        };

        fun({ trade });
      }
    });
    return self;
  }

  onOpen(fun = () => {}) {
    const self = this;
    self.webSocket.on('open', (open) => {
      self.opened = true;
      self.subscribeTrades();
      fun(open);
    });
    return self;
  }

  subscribeTrades() {
    const self = this;
    if (!self.opened) return self;
    const event = JSON.stringify({
      event: 'subscribe',
      channel: 'trades',
      symbol: PAIR,
    });
    self.webSocket.send(event);
    return self;
  }
}

module.exports = {
  /** Public */
  getCandles,
  /** WebSocket */
  WS,
};
