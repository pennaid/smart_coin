/* eslint-disable camelcase */
const axios = require('axios');
const WebSocket = require('ws');
const moment = require('moment');

const { Candle, logger } = require('../helpers');

const WS_URL = 'wss://ws-feed.gdax.com/';
const API_URL = 'https://api.gdax.com/';
const PAIR = 'BTC-USD';

const PublicRequest = axios.create({ baseURL: API_URL });

const getCandles = async(num = 32, { granularity = 14400 } = {}) => {
  const refDate = moment()
    .subtract(num * granularity, 'seconds');
  refDate
    .set('hour', (Math.floor(refDate.get('hour') / 4) * 4) - 4)
    .set('minute', 0)
    .set('seconds', 0)
    .set('milliseconds', 0);


  const endDate = moment().toISOString();
  const startDate = refDate.toISOString();

  const query = {
    method: 'GET',
    url: `products/${PAIR}/candles`,
    params: {
      start: startDate,
      end: endDate,
      granularity,
    },
    timeout: 5000,
  };

  try {
    const candles = (await PublicRequest(query)).data;

    const mappedCandles = candles.map((candle) => {
      const time = Number(`${candle[0]}000`);
      const low = Math.abs(Number(candle[1]));
      const high = Math.abs(Number(candle[2]));
      const open = Math.abs(Number(candle[3]));
      const close = Math.abs(Number(candle[4]));
      const volume = Math.abs(Number(candle[5]));

      const CandleInstance = new Candle();

      return CandleInstance
        .setClose(close, time)
        .setLow(low)
        .setHigh(high)
        .setOpen(open)
        .setVolume(volume);
    }).reverse().slice(candles.length - num, candles.length);

    return mappedCandles;
  } catch (e) {
    logger.error('GDAX candles timeout (3000ms). Retrying.\n', e.name);
    return getCandles.apply(this, arguments);
  }
};

/** WebSocket */

class WS {
  constructor() {
    const gdaxSocket = new WebSocket(WS_URL);
    this.webSocket = gdaxSocket;
    this.onOpen();
    this.opened = false;
  }

  onMessage(fun = () => {}) {
    const self = this;
    self.webSocket.on('message', (message) => {
      const parsed = JSON.parse(message);
      const { trade_id, time, price: stringPrice, last_size } = parsed;

      if (stringPrice && time) {
        const id = trade_id;
        const timestamp = new Date(time).getTime();
        const amount = Math.abs(Number(last_size));
        const price = Math.abs(Number(stringPrice));
        const trade = {
          id,
          timestamp,
          amount,
          price,
        };

        fun({ trade });
      }
    });
    return self;
  }

  onOpen(fun = () => {}) {
    const self = this;
    self.webSocket.on('open', (open) => {
      self.opened = true;
      self.subscribeTrades();
      fun(open);
    });
    return self;
  }

  subscribeTrades() {
    const self = this;
    if (!self.opened) return self;
    const event = JSON.stringify({
      type: 'subscribe',
      product_ids: [PAIR],
      channels: ['ticker'],
    });
    self.webSocket.send(event);
    return self;
  }
}

module.exports = {
  /** Public */
  getCandles,
  /** WebSocket */
  WS,
};
