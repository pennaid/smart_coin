FROM node:8.3.0-alpine

RUN mkdir -p /usr/app
WORKDIR /usr/app
ADD ./package.json .
RUN npm install && npm cache clean --force
ADD ./src ./src

# CMD npm start
