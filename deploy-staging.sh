export HEROKU_APP_NAME="smartcoin-staging"
docker-compose up --build -d smartcoin-core
docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
docker push registry.heroku.com/$HEROKU_APP_NAME/web
docker-compose stop